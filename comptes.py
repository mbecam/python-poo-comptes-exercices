"""
    Classes pour la gestion de comptes bancaires
"""

from abc import ABC, abstractmethod

AUTORISATION_DECOUVERT = -100
POURCENTAGE_AGIOS = 5
POURCENTAGE_INTERETS = 1

class Compte():
    """
        Classe abstraite pour représenter un compte bancaire générique.
    """

    def __init__(self, numeroCompte: int, nomProprietaire: str, solde: int = 0):
        self.__numeroCompte = numeroCompte
        self.__nomProprietaire = nomProprietaire
        self.__solde = solde

    def get_solde(self):
        return self.__solde

    def set_solde(self, solde):
        self.__solde = solde

    @abstractmethod
    def retrait(self, montant):
        pass

    @abstractmethod
    def versement(self, montant):
        pass

    def afficherSolde(self):
        print("Solde du compte : {:0.2f}".format(self.__solde))


class CompteCourant(Compte):
    """
        Classe pour représenter un compte courant qui prend en charge le découvert et les agios.
    """

    def __init__(self, numeroCompte: int, nomProprietaire: str, solde: int = 0):
        super().__init__(numeroCompte, nomProprietaire, solde)
        self.__autorisationDecouvert = AUTORISATION_DECOUVERT
        self.__pourcentageAgios = POURCENTAGE_AGIOS

    def appliquerAgios(self):
        self.afficherSolde()
        print("Votre solde est négatif, une pénalité de {}% va être appliquée.".format(self.__pourcentageAgios))
        montant = self.get_solde()*(self.__pourcentageAgios/100)
        self.set_solde(self.get_solde() + montant)
        self.afficherSolde()

    def retrait(self, montant):
        self.afficherSolde()
        if self.get_solde() + montant < self.__autorisationDecouvert:
            print("Vous ne pouvez pas retirez {}, vous dépasseriez votre autorisation de découvert de {}"\
                  .format(montant, self.__autorisationDecouvert))
        else:
            self.set_solde(self.get_solde() + montant)
            print("Vous avez retirez {} de ce compte courant".format(montant))
            self.afficherSolde()
            if self.get_solde() < 0:
                print("Attention, ce compte est désormais à découvert !")
                self.appliquerAgios()

    def versement(self, montant):
        self.afficherSolde()
        self.set_solde(self.get_solde() + montant)
        print("Vous avez versé {} sur ce compte courant".format(montant))
        self.afficherSolde()
        if self.get_solde() < 0:
            self.appliquerAgios()


class CompteEpargne(Compte):
    """
        Classe pour représenter un compte d'épargne qui ajoute des intérêts au solde du compte.
    """

    def __init__(self, numeroCompte: int, nomProprietaire: str, solde: int = 0):
        super().__init__(numeroCompte, nomProprietaire, solde,)
        self.__pourcentageInterets = POURCENTAGE_INTERETS

    def appliquerInterets(self):
        self.afficherSolde()
        print("Des intérêts de {}% vont être ajoutés à votre solde.".format(self.__pourcentageInterets))
        montant = self.get_solde()*(self.__pourcentageInterets/100)
        self.set_solde(self.get_solde() + montant)
        self.afficherSolde()

    def retrait(self, montant):
        self.afficherSolde()
        if self.get_solde() + montant >= 0:
            self.set_solde(self.get_solde() + montant)
            print("Vous avez retiré {} de ce compte épargne.".format(montant))
            self.afficherSolde()
            self.appliquerInterets()
        else:
            print("Vous ne pouvez pas retirer {} (montant trop élevé).".format(montant))

    def versement(self, montant):
        self.afficherSolde()
        self.set_solde(self.get_solde() + montant)
        print("Vous avez versé {} sur ce compte épargne.".format(montant))
        self.appliquerInterets()

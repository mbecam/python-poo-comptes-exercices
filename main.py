"""
    Implémentation d'un fonctionnement bancaire de gestion de comptes.
"""

from comptes import CompteEpargne
from comptes import CompteCourant


def gestion_comptes(comptes: []):
    """
        Interface utilisateur pour effectuer des opérations sur des comptes bancaires
        :param comptes: liste des comptes bancaires du propriétaire
    """
    choix_action = -1
    choix_compte = -1
    quitter = False

    while quitter is False:
        if choix_compte == -1:
            try:
                temp = int(input("Voulez-vous voir un compte ?\
                                    \n Tapez 0 pour le compte courant\
                                    \n Tapez 1 pour le compte epargne\
                                    \n Tapez 2 pour quitter\n"))
                # if not 0 <= temp <= 2:
                #     print("Renseignez un entier correspondant à une des options disponibles")
            except ValueError:
                print("Vous n'avez pas renseigner un entier")
            else:
                choix_compte = temp
            quitter = (choix_compte == 2)
        else:
            try:
                choix_action = int(input("Nouvelle opération banquaire. Quelle action voulez-vous faire ?\
                                     \n Tapez 0 pour revenir au choix du compte\
                                     \n Tapez 1 pour effectuer un versement\
                                     \n Tapez 2 pour effectuer un retrait\
                                     \n Tapez 3 pour quitter\n"))
            except ValueError:
                print("Vous n'avez pas renseigner un entier")
            quitter = (choix_action == 3)
            if choix_action == 1:
                try:
                    montant = float(input("Renseignez le montant de votre versement (nombre positif) : "))
                    if montant < 0:
                        print("Vous n'avez pas donné de montant positif pour le versement.")
                    else:
                        comptes[choix_compte].versement(montant)
                except ValueError:
                    print("Renseignez un nombre pour le montant de la transaction.")
            elif choix_action == 2:
                try:
                    montant = float(input("Renseignez le montant de votre retrait (nombre négatif) : "))
                    if montant > 0:
                        print("Vous n'avez pas donné de montant négatif pour le retrait.")
                    else:
                        comptes[choix_compte].retrait(montant)
                except ValueError:
                    print("Renseignez un nombre pour le montant de la transaction.")
            elif not choix_action:
                choix_compte = -1
            else:
                print("Vous n'avez pas proposé d'entier correspondant à une action disponible")


if __name__ == "__main__":
    # création de comptes
    proprietaire = "Maud"
    compte_courant = CompteCourant(1, proprietaire, 100)
    compte_epargne = CompteEpargne(2, proprietaire, 100)

    mes_comptes = [compte_courant, compte_epargne]
    gestion_comptes(mes_comptes)


